class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.date :date
      t.string :reason
      t.integer :km_nbr
      t.integer :tax_coef
      t.integer :journey
      t.text :departure
      t.text :arrival

      t.timestamps null: false
    end
  end
end
