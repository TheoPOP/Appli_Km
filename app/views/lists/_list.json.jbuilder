json.extract! list, :id, :date, :reason, :km_nbr, :tax_coef, :journey, :departure, :arrival, :created_at, :updated_at
json.url list_url(list, format: :json)